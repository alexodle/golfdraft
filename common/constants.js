'use strict';

module.exports = {
  // Tournaments are 4 days
  NDAYS: 4,

  // Each player can draft 4 golfers
  NGOLFERS: 4,

  MISSED_CUT: 'MC'
};
