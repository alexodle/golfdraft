'use strict';

var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  CURRENT_USER_CHANGE: null,
  SET_GOLFERS: null,
  SET_PLAY_SOUNDS: null,
  SET_PLAYERS: null,
  SET_USERS: null
});
