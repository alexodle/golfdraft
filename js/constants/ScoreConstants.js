'use strict';

var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  SCORE_UPDATE: null
});
