'use strict';

var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  CREATE_MESSAGE: null,
  NEW_MESSAGE: null,
  SET_MESSAGES: null
});
