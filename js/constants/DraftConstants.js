'use strict';

var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
  DRAFT_PICK: null,
  DRAFT_UPDATE: null
});
