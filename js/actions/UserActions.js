'use strict';

var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var UserActions = {

  setCurrentUser: function (user) {
    AppDispatcher.handleViewAction({
      actionType: AppConstants.CURRENT_USER_CHANGE,
      currentUser: user
    });
  },

  /**
   Same as setting the current user, except that this is specifically
   reserved for app startup
   */
  hydrateCurrentUser: function (user) {
    AppDispatcher.handleServerAction({
      actionType: AppConstants.CURRENT_USER_CHANGE,
      currentUser: user,
      doNotSync: true
    });
  }

};

module.exports = UserActions;
